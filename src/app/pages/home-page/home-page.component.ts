import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';

import { UserFormComponent } from 'src/app/components/user-form/user-form.component';
import { UserCountComponent } from 'src/app/components/user-count/user-count.component';
import { DisplayUsersComponent } from 'src/app/components/display-users/display-users.component';

import { ManageUserdataService } from 'src/app/services/manage-userdata.service';
import { map } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css'],
})
export class HomePageComponent implements OnInit {
  fetchedUsers = [];
  editUserId; //Dynamic userId changes value according to edit user method

  @ViewChild(UserFormComponent) userFormComp: UserFormComponent;
  @ViewChild(UserCountComponent) userCountComp: UserCountComponent;
  @ViewChild(DisplayUsersComponent)
  displayUsersComponent: DisplayUsersComponent;

  @ViewChild('displayTable') displayTable: ElementRef;
  @ViewChild('userForm') userForm: ElementRef;

  constructor(
    private userService: ManageUserdataService,
    private toastr: ToastrService
  ) {}

  ngOnInit() {
    this.getUserCount();
  }

  /* Add User To The Database */
  onAddUser(user) {
    this.userService.addUser(user).subscribe(
      (res) => {
        this.getUserCount();
        this.toastr.success('User added successfully');
        this.onFetchUsers();
      },
      (err) => {
        let valError = err.error.map((message) => message.message);
        this.toastr.error(
          this.displayValidationError(valError),
          'Validation Error'
        );
      }
    );
  }

  /* Get All Users Data From Database */
  onFetchUsers() {
    this.userCountComp.userLoading = true;
    this.displayUsersComponent.databaseTitle = this.userService.getDatabaseTitle();

    this.userService
      .fetchUsers()
      .pipe(
        map((resData) => {
          const userArray = [];
          for (const key in resData) {
            if (resData.hasOwnProperty(key)) {
              userArray.push({ userId: key, ...resData[key] });
            }
          }
          return userArray;
        })
      )
      .subscribe(
        (res) => {
          this.fetchedUsers = res;
          this.displayUsersComponent.isUser = true;
          this.userCountComp.userLoading = false;
          setTimeout(() => (this.scrollToElement(this.displayTable), 100));
        },
        (err) => {
          this.toastr.error(err.message, 'Something went wrong');
          this.userCountComp.userLoading = false;
        }
      );
  }

  /* Transfers Data To User Form To Edit */
  onEditUser(data) {
    this.userFormComp.editMode = true;
    this.editUserId = data.userId;
    this.scrollToElement(this.userForm);

    let i = data.index;
    this.userFormComp.userDataForm.setValue({
      firstName: this.fetchedUsers[i].firstName,
      lastName: this.fetchedUsers[i].lastName,
      email: this.fetchedUsers[i].email,
      country: this.fetchedUsers[i].country,
      gender: this.fetchedUsers[i].gender,
    });
    this.toastr.info('Please make required changes to update the user data');
  }

  /* Updates Existing User Record */
  onUpdateUser(user) {
    this.userService.updateUser(this.editUserId, user).subscribe(
      (res) => {
        this.onFetchUsers();
        this.toastr.success('User updated successfully');
      },
      (err) => {
        let valError = err.error.map((message) => message.message);
        this.toastr.error(
          this.displayValidationError(valError),
          'Validation Error'
        );
      }
    );
  }

  /* Deletes Single User Record From Database */
  onDeleteUser(userId) {
    if (confirm('Do you want to delete this user ?')) {
      this.userService.deleteUser(userId).subscribe(
        (res) => {
          this.getUserCount();
          this.onFetchUsers();
          this.toastr.warning('User deleted successfully');
        },
        (err) => {
          this.toastr.error(err.message, 'Something went wrong');
        }
      );
    }
  }

  /* Fetch Random User Data */
  onGenerateRandomUser() {
    this.userFormComp.isRandomUserLoading = true;
    this.userService.getRandomUser().subscribe(
      (res: any) => {
        this.userFormComp.randomUser = res.results[0];
        this.fillRandomUserData();
        this.userFormComp.isRandomUserLoading = false;
        this.userFormComp.editMode = false;
      },
      (err) => {
        this.toastr.error(err.message, 'Something went wrong');
        this.userFormComp.isRandomUserLoading = false;
      }
    );
  }

  /* Gets Total User Count From The Database */
  getUserCount() {
    this.userService.fetchUsers().subscribe(
      (res) => {
        this.userCountComp.userCount = Object.keys(res).length;
      },
      (err) => this.toastr.error(err.message, 'Cannot load User Count')
    );
  }

  /* Method To Fill Random User Data In User Form */
  fillRandomUserData() {
    this.userFormComp.userDataForm.setValue({
      firstName: this.userFormComp.randomUser.name?.first,
      lastName: this.userFormComp.randomUser.name?.last,
      email: this.userFormComp.randomUser.email,
      country: this.userFormComp.randomUser.location?.country,
      gender: this.userFormComp.randomUser.gender,
    });
  }

  /* Scrolls To Particular Element */
  scrollToElement(element: ElementRef) {
    element.nativeElement.scrollIntoView({
      behavior: 'smooth',
      block: 'start',
    });
  }

  displayValidationError(error) {
    let valError = '';
    for (let index in error) {
      valError += error[index] + '\n';
    }
    return valError;
  }
}
