import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-user-count',
  templateUrl: './user-count.component.html',
  styleUrls: ['./user-count.component.css'],
})
export class UserCountComponent implements OnInit {
  userCount: number;
  userLoading: boolean;

  @Output() fetchUser: EventEmitter<any> = new EventEmitter();

  constructor() {}

  ngOnInit(): void {}

  onFetchCallParent() {
    this.fetchUser.next();
  }
}
