import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { faUserEdit, faTrashAlt } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-display-users',
  templateUrl: './display-users.component.html',
  styleUrls: ['./display-users.component.css'],
})
export class DisplayUsersComponent implements OnInit {
  faUserEdit = faUserEdit;
  faTrashAlt = faTrashAlt;
  isUser: boolean;
  databaseTitle;

  @Input() users = [];
  @Output() deleteUser = new EventEmitter<any>();
  @Output() editUser = new EventEmitter<{ userId: number; index: number }>();

  constructor() {}

  ngOnInit(): void {}

  onDeleteCallParent(userId: number) {
    this.deleteUser.emit(userId);
  }

  onEditCallParent(userId: number, index: number) {
    this.editUser.emit({ userId: userId, index: index });
  }
}
