import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { countryList } from 'src/utilities/arrays';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.css'],
})
export class UserFormComponent implements OnInit {
  userDataForm: FormGroup;
  countries = countryList;
  randomUser: any;
  isRandomUserLoading: boolean = false;
  // validationError: [''];
  // isFieldValid: boolean;

  submitted: boolean = false;
  editMode: boolean;

  @Output() addUser = new EventEmitter<any>();
  @Output() updateUser = new EventEmitter<any>();
  @Output() generateRandomUser = new EventEmitter<any>();

  constructor(private formbuilder: FormBuilder) {}

  ngOnInit(): void {
    this.userDataForm = this.formbuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      country: ['', Validators.required],
      gender: ['', Validators.required],
    });
  }

  onAddCallParent(userData) {
    this.submitted = true;
    this.addUser.emit(userData);
    this.onReset();
  }

  onUpdateCallParent(userData) {
    this.updateUser.emit(userData);
    this.onReset();
  }

  onGenerateCallParent() {
    this.generateRandomUser.emit();
  }

  onReset() {
    this.submitted = false;
    this.userDataForm.reset();
    this.editMode = false;
  }

  /* Form Validation */
  get val() {
    return this.userDataForm.controls;
  }

  errorFirstName() {
    return (
      this.submitted ||
      (this.val.firstName.touched && this.val.firstName.errors?.required)
    );
  }

  errorLastName() {
    return (
      this.submitted ||
      (this.val.lastName.touched && this.val.lastName.errors?.required)
    );
  }

  errorEmail() {
    return (
      this.submitted ||
      (this.val.email.touched && this.val.email.errors?.required)
    );
  }

  errorCountry() {
    return (
      this.submitted ||
      (this.val.country.touched && this.val.country.errors?.required)
    );
  }

  errorGender() {
    return (
      this.submitted ||
      (this.val.gender.touched && this.val.gender.errors?.required)
    );
  }

  // FieldValidationMessage(field) {
  //   return this.validationError.indexOf(field) > -1;
  // }
}
