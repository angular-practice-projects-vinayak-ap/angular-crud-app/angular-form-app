export interface UserModel {
  userId?: string;
  firstName: string;
  lastName: string;
  email: string;
  country: string;
  gender: string;
}
