import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UserModel } from 'src/app/model/user.model';

@Injectable({
  providedIn: 'root',
})
export class ManageUserdataService {
  url = '/api/users';

  constructor(private http: HttpClient) {}

  addUser(users: any[]) {
    return this.http.post<UserModel>(this.url, users);
  }

  fetchUsers() {
    return this.http.get<UserModel>(this.url);
  }

  deleteUser(uid) {
    return this.http.delete(`/api/users/${uid}`);
  }

  updateUser(uid, userData) {
    return this.http.put<UserModel>(`/api/users/${uid}`, userData);
  }

  getRandomUser() {
    return this.http.get<UserModel>('/api/randomUser');
  }

  getDatabaseTitle() {
    return this.http.get('/api/dataTitle');
  }
}
